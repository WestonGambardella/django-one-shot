from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoFormItem

# Create your views here.
def show_todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list
    }
    return render(request, "todos/list.html", context)

def show_todo_detail(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": todo_list_detail
    }
    return render(request, "todos/detail.html", context)

def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("show_todo_detail", id=item.id)

    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def edit_todo(request, id):
    edit_todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=edit_todo_list)
        if form.is_valid():
            edit_todo_list = form.save()
            return redirect("show_todo_detail", id=edit_todo_list.id)

    else:
        form = TodoForm(instance=edit_todo_list)
    context = {
        "form": form
    }
    return render(request, "todos/edit.html", context)

def delete_todo(request, id):
    deleted = TodoList.objects.get(id=id)
    if request.method == "POST":
        deleted.delete()
        return redirect("show_todo_list")
    return render(request, "todos/delete.html")

def create_task(request):
    if request.method == "POST":
        form = TodoFormItem(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("show_todo_detail", id=item.list.id)

    else:
        form = TodoFormItem()
    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)

def edit_todoitem(request, id):
    edit_todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoFormItem(request.POST, instance=edit_todo_item)
        if form.is_valid():
            edit_todo_item = form.save()
            return redirect("show_todo_detail", id=edit_todo_item.id)

    else:
        form = TodoFormItem(instance=edit_todo_item)
    context = {
        "form": form
    }
    return render(request, "todos/edititem.html", context)
