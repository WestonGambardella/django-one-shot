from django.urls import path
from todos.views import show_todo_list, show_todo_detail, create_todo, edit_todo, delete_todo, create_task, edit_todoitem

urlpatterns = [
    path("", show_todo_list, name="show_todo_list"),
    path("<int:id>/", show_todo_detail, name="show_todo_detail"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/edit/", edit_todo, name="edit_todo"),
    path("<int:id>/delete/", delete_todo, name="delete_todo"),
    path("items/create", create_task, name="create_task"),
    path("items/<int:id>/edit/", edit_todoitem, name="edit_todoitem"),
]
